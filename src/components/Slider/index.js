import TopBanners from '../TopBanners/index';
const Slider = () => {
    return (
        <div className="img-wrapper">
            <img src="Fuji_TallHero_45M_v2_2x._CB432458382_.jpg" alt="slider" />

            <div className="banner-backgroung">
                <TopBanners />
            </div>

        </div>
    )
}

export default Slider;