

const Blocks = () => {
    return (
        <div className="banner-div">
            <h1>
                Upgrade your home | Amazon Brands & more
            </h1>
            <div className="row">
                <div className="col-md-6">
                    <img src="xcm_banners_tvs_372x232_372x232_in-en._SY232_CB663888090_.jpg" alt="Smart LED TVs" />
                    <span>Smart LED TVs</span>
                </div>
                <div className="col-md-6">
                    <img src="xcm_banners_la_372x232_372x232_in-en._SY232_CB663888089_.jpg" alt="Appliances" />
                    <span>Appliances</span>
                </div>
                <div className="col-md-6">
                    <img src="xcm_banners_furn_372x232_372x232_in-en._SY232_CB663888091_.jpg" alt="Furniture" />
                    <span>Furniture</span>
                </div>
                <div className="col-md-6">
                    <img src="xcm_banners_kitchen_372x232_372x232_in-en._SY232_CB663888091_.jpg" alt="Kitchen products" />
                    <span>Kitchen products</span>
                </div>
                
            </div>

            <a href="#">Shop Now</a>
            
        </div>
    )
}

export default Blocks;