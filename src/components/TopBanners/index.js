
import Blocks from './Blocks';
const TopBanners = () => {
    return (
        <div className="blocks-wrapper">
            <div className="container-fluid">
                <div className="row">
                    <div className="col-md-3">
                        <Blocks />
                    </div>
                    <div className="col-md-3">
                        <Blocks />
                    </div>
                    <div className="col-md-3">
                        <Blocks />
                    </div>
                    <div className="col-md-3">
                        Other
                    </div>
                </div>
            </div>
        </div>
    )
}
export default TopBanners;