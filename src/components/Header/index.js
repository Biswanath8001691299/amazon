import React from 'react';
import HeaderLogo from './conponents/HeaderLogo';
import DeliveryLoocation from './conponents/DeliveryLoocation';
import Search from './conponents/Search';
import OrderReturn from './conponents/OrderReturn';
import SignIn from './conponents/SignIn';
import Cart from './conponents/Cart';

const Header = () => {
    return (
        <React.Fragment>
            <div className="header">
                <HeaderLogo />
                <DeliveryLoocation />
                <Search />
                <SignIn />
                <OrderReturn />
                <Cart />
            </div>
            <div className="bottom-div-wrapper">
                <div className="bottom-div">
                    <div>
                        <span>All</span>
                    </div>
                    <div>
                        <span>Today's Deal</span>
                    </div>
                    <div>
                        <span>Customer Service</span>
                    </div>
                    <div>
                        <span>Gift Cards</span>
                    </div>
                    <div>
                        <span>Registry</span>
                    </div>
                    <div>
                        <span>Sell</span>
                    </div>
                </div>
                <div>
                    <span>Amazon's response to COVID-19</span>
                </div>
            </div>
        </React.Fragment>

    )
}
export default Header;