import { BsSearch } from "react-icons/bs";
import { Input } from "reactstrap";
const Search = () => {
    return (
        <div className="header-search">
            <Input type="select" name="select" id="exampleSelect">
                <option>All</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
            </Input>
            <input className="" type="text" />
            <button className="" type="submit"><BsSearch /></button>
        </div>
    )
}

export default Search;