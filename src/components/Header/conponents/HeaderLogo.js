const Logo = () => {

    return (
        <div className="logo">
            <img className="img-logo" src="amazon_logo.jpg" alt="logo" />
        </div>
    )
}

export default Logo;