import { BsGeoAlt } from "react-icons/bs";

const DeliveryLoocation = () => {
    return (
        <div className="address-container">
            <div className="address-icon">
                <BsGeoAlt />
            </div>
            <div>
                <span>Deliver To Biswanath</span>
                <h3>Contai 721444</h3>
            </div>
        </div>
    )
}
export default DeliveryLoocation;